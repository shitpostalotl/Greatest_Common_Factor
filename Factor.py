def euclids_algorithem(a,b):
	try:
		a = int(a)
		b = int(b)

		while a != b:
			if a == b:
				return a
			if a > b:
				a = a-b
			elif b > a:
				b = b-a
		return a

	except TypeError:
		return("Those aren't numbers")

if __name__ == "__main__":
	a = input("Enter the first number: ")
	b = input("Enter the second number: ")
	print(euclids_algorithem(a,b))
